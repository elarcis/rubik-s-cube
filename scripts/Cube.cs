using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Elarcis.RubiksCube
{
    [Tool, Icon("assets/cube.svg")]
    public partial class Cube : Node3D
    {
        [Export]
        public PackedScene CornerPiece { get; set; }

        [Export]
        public PackedScene SidePiece { get; set; }

        [Export]
        public PackedScene CenterPiece { get; set; }

        private const int SIDES_PER_FACE_COUNT = 4;
        private const int CELLS_PER_SIDE_COUNT = 3;

        public override void _Ready()
        {
            CreatePieces();
        }

        private void CreatePieces()
        {
            if (CenterPiece != null)
            {
                var centers = Enumerable.Range(0, 6).Select(AddCenter).ToList();
            }
            if (CornerPiece != null)
            {
                var corners = Enumerable.Range(0, 8).Select(AddCorner).ToList();
            }
            if (SidePiece != null)
            {
                var sides = Enumerable.Range(0, 12).Select(AddSide).ToList();
            }
        }

        private Node3D AddCenter(int index)
        {
            var center = CenterPiece.Instantiate<Node3D>();
            center.Name = $"Center{index}";
            AddChild(center);
            if (index >= SIDES_PER_FACE_COUNT)
            {
                center.GlobalRotate(Vector3.Right, Mathf.Pi * ((index * 2) - 9) / 2);
                // Transform indices 4/5 into -1/1 multipliers  ^^^^^^^^^^^^^^
            }
            else
            {
                center.GlobalRotate(Vector3.Up, Mathf.Pi * index / 2);
            }
            return center;
        }

        private Node3D AddCorner(int index)
        {
            var corner = CornerPiece.Instantiate<Node3D>();
            corner.Name = $"Corner{index}";
            AddChild(corner);
            int row = index / SIDES_PER_FACE_COUNT % 2;
            corner.GlobalRotate(Vector3.Back, Mathf.Pi * row);
            corner.GlobalRotate(Vector3.Up, Mathf.Pi * (index % SIDES_PER_FACE_COUNT) / 2);
            return corner;
        }

        private Node3D AddSide(int index)
        {
            var side = SidePiece.Instantiate<Node3D>();
            side.Name = $"Side{index}";
            AddChild(side);
            int row = index / SIDES_PER_FACE_COUNT % CELLS_PER_SIDE_COUNT;
            side.GlobalRotate(Vector3.Back, Mathf.Pi * row / 2);
            side.GlobalRotate(Vector3.Up, Mathf.Pi * (index % SIDES_PER_FACE_COUNT) / 2);
            return side;
        }
    }
}
